CC := /usr/bin/avr-gcc
CXX := /usr/bin/avr-g++
AR := /usr/bin/avr-ar
OBJCOPY := /usr/bin/avr-objcopy
AVRDUDE := /usr/bin/avrdude

TARGET_EXEC ?= logic_puzzle.elf

BUILD_DIR ?= ./build
SRC_DIRS ?= ./src
ARDUINO_SRC_DIR := /usr/share/arduino/hardware/arduino/cores/arduino
ARDUINO_INCLUDES := /usr/share/arduino/hardware/arduino/cores/arduino /usr/share/arduino/hardware/arduino/variants/standard

AVRDUDE_CONF := /usr/share/arduino/hardware/tools/avrdude.conf

SRCS := $(shell find $(SRC_DIRS) -name *.cpp -or -name *.c -or -name *.s)
ARDUINO_SRCS := $(shell find $(ARDUINO_SRC_DIR) -name *.cpp -or -name *.c -or -name *.s)
OBJS := $(SRCS:%=$(BUILD_DIR)/%.o) $(ARDUINO_SRCS:%=$(BUILD_DIR)/%.o)
DEPS := $(OBJS:.o=.d)

INC_DIRS := $(ARDUINO_INCLUDES)
INC_DIRS += $(shell find $(SRC_DIRS) -type d)
INC_FLAGS := $(addprefix -I,$(INC_DIRS))

MCU ?= atmega328p
COMMON_FLAGS ?= $(INC_FLAGS) -MMD -MP -mmcu=$(MCU) -DF_CPU=16000000L -Wall -Os
CXXFLAGS += -std=c++11
LDFLAGS += -Wl,--gc-sections -Os -lc -lm -mmcu=$(MCU)

$(BUILD_DIR)/$(TARGET_EXEC): $(OBJS) $(ARDUINO_OBJS)
	$(CC) $(OBJS) -o $@ $(LDFLAGS)
	$(OBJCOPY) -O ihex -R .eeprom $@ $(@:.elf=.hex)
	$(OBJCOPY) -O binary -R .eeprom $@ $(@:.elf=.bin)

# assembly
$(BUILD_DIR)/%.s.o: %.s
	$(MKDIR_P) $(dir $@)
	$(AS) $(COMMON_FLAGS) $(ASFLAGS) -c $< -o $@

# c source
$(BUILD_DIR)/%.c.o: %.c
	$(MKDIR_P) $(dir $@)
	$(CC) $(COMMON_FLAGS) $(CFLAGS) -c $< -o $@

# c++ source
$(BUILD_DIR)/%.cpp.o: %.cpp
	$(MKDIR_P) $(dir $@)
	$(CXX) $(COMMON_FLAGS) $(CXXFLAGS) -c $< -o $@

.PHONY: clean
clean:
	$(RM) -r $(BUILD_DIR)

.PHONY: upload
upload: check_avr_com
	${AVRDUDE} -v -C $(AVRDUDE_CONF) -p$(MCU) -c arduino -P$(AVR_COM) -b115200 -U flash:w:$(BUILD_DIR)/$(TARGET_EXEC:.elf=.hex):i

.PHONY: check_avr_com
check_avr_com:
ifndef AVR_COM
	$(error Error: AVR_COM must be set e.g: 'make upload AVR_COM=/dev/ttyACM0')
endif

-include $(DEPS)

MKDIR_P ?= mkdir -p
