# stem-logic-puzzle

Arduino Uno project to control the "Logic Circuit" puzzle. The project uses a GNU Makefile with avr-gcc to build for the atmega328p.

## Hardware


## Software

### Files

- logic_puzzle.cpp: the main code file, controlling the puzzle
- solver.cpp/hpp: helper class containing puzzle definitions and Solve() function

### Setup

- Install Arduino toolchain (sudo apt-get install arduino) to get
  avr-gcc and the required Arduino headers/libs
- Install avrdude (sudo apt-get install avrdude) which is the Arduino programming tool

### Building 

Simply run "make" inside the project directory. You may need to override the following Makefile vars:

- CC, CXX, AS, LD and OBJCOPY: must point to your install of avr-gcc
- ARDUINO_SRC_DIR: Arduino code files (default: /usr/share/arduino/hardware/arduino/cores/arduino)
- ARDUINO_INCLUDES: Arduino header files (/usr/share/arduino/hardware/arduino/cores/arduino and /usr/share/arduino/hardware/arduino/variants/standard)

The output will be generated in the "build" directory. The .hex file should be supported by the Arduino USB bootloader.

### Programming

Run "make upload AVR_COM=XXXXX", where XXXX is the COM port connected to the Arduino. This will upload the .hex file generated to the Arduino. You may need to override the following Makefile vars:

- AVRDUDE: must point to your install of avrdude
