// File: solver.hpp
// Author: Tom Woolfrey
//
// Puzzle solver class header

#ifndef SOLVER_HPP
#define SOLVER_HPP

#include <stdint.h>

#define PUZZLE_INPUT_COUNT 8U
#define PUZZLE_OUTPUT_COUNT 8U

class tclsNode
{
    public:

        enum class NodeType
        {
            False = 0,
            True = 1,
            Literal = 2,
            Not = 3,
            And = 4,
            Or = 5,
            Nand = 6,
            Nor = 7,
            Xor = 8
        };

        tclsNode() :
            m_pcu8Descriptor(nullptr),
            m_bStateDetermined(false),
            m_bState(false)
        {
        }
        
        void SetDescriptor(const uint8_t *const cpcu8Descriptor)
        {
            m_pcu8Descriptor = cpcu8Descriptor;
        }

        bool DetermineState( const bool (&crefabInputState)[PUZZLE_INPUT_COUNT], tclsNode *componentList, const uint8_t cu8ComponentCount )
        {
            if(!m_bStateDetermined)
            {
                if(m_pcu8Descriptor != nullptr)
                {
                    // Descriptor layout is as follows:
                    // desc[0]: Node type
                    // desc[1]: Child count
                    // desc[2]: Child 1 index
                    // ...
                    // desc[2+N-1]: Child N-1 index

                    const uint8_t cu8NodeType = m_pcu8Descriptor[0U];
                    switch(cu8NodeType)
                    {
                        case static_cast<uint8_t>(NodeType::False):
                        {
                            m_bState = false;
                            break;
                        }
                        case static_cast<uint8_t>(NodeType::True):
                        {
                            m_bState = true;
                            break;
                        }
                        case static_cast<uint8_t>(NodeType::Literal):
                        {
                            // Node has 1 child - ignore child_count field

                            uint16_t u16InputNum = m_pcu8Descriptor[2U];
                            if(u16InputNum < PUZZLE_INPUT_COUNT)
                            {
                                m_bState = crefabInputState[u16InputNum];
                            }
                            break;
                        }
                        case static_cast<uint8_t>(NodeType::Not):
                        {
                            // Node has 1 child - ignore child_count field
                            const uint8_t cu8ChildComponentNum = m_pcu8Descriptor[2U];

                            if(cu8ChildComponentNum < cu8ComponentCount)
                            {
                                m_bState = !componentList[ cu8ChildComponentNum ].DetermineState( crefabInputState, componentList, cu8ComponentCount );
                            }
                            break;
                        }
                        case static_cast<uint8_t>(NodeType::And):
                        {
                            // AND gate is true if all children are true
                            const uint8_t cu8ChildCount = m_pcu8Descriptor[1U];
                            m_bState = (CountTrueChildren( crefabInputState, componentList, cu8ComponentCount ) == cu8ChildCount);
                            break;
                        }
                        case static_cast<uint8_t>(NodeType::Or):
                        {
                            // OR gate is true if at least one child is true
                            m_bState = (CountTrueChildren( crefabInputState, componentList, cu8ComponentCount ) != 0U);
                            break;
                        }
                        case static_cast<uint8_t>(NodeType::Nand):
                        {
                            // NAND gate is true if not all children are true
                            const uint8_t cu8ChildCount = m_pcu8Descriptor[1U];
                            m_bState = (CountTrueChildren( crefabInputState, componentList, cu8ComponentCount ) != cu8ChildCount);
                            break;
                        }
                        case static_cast<uint8_t>(NodeType::Nor):
                        {
                            // NOR gate is true if no children are true
                            m_bState = (CountTrueChildren( crefabInputState, componentList, cu8ComponentCount ) == 0U);
                            break;
                        }
                        case static_cast<uint8_t>(NodeType::Xor):
                        {
                            // XOR gate is true if exactly one child is true
                            m_bState = (CountTrueChildren( crefabInputState, componentList, cu8ComponentCount ) == 1U);
                            break;
                        }
                        default:
                        {
                            break;
                        }
                    }
                }

                m_bStateDetermined = true;
            }
            
            return m_bState;
        }

        uint8_t CountTrueChildren( const bool (&crefabInputState)[PUZZLE_INPUT_COUNT], tclsNode *componentList, const uint8_t cu8ComponentCount )
        {
            // Node has N children
            const uint8_t cu8ChildCount = m_pcu8Descriptor[1U];
            uint8_t u8TrueCount = 0U;

            for(uint8_t i = 0U; i < cu8ChildCount; i++)
            {
                const uint8_t cu8ChildComponentNum = m_pcu8Descriptor[2U + i];
                if(cu8ChildComponentNum < cu8ComponentCount)
                {
                    // As soon as m_bState(false),the first child that is TRUE is reached, we are done and can break out
                    if(componentList[ cu8ChildComponentNum ].DetermineState( crefabInputState, componentList, cu8ComponentCount ) == true)
                    {
                        u8TrueCount++;
                    }
                }
            }

            return u8TrueCount;
        }

        void Reset()
        {
            m_bStateDetermined = false;
        }

    private:

        const uint8_t *m_pcu8Descriptor;
        bool m_bStateDetermined;
        bool m_bState;
};

class tclsSolver
{
    public:

        static constexpr uint8_t s_cu8PuzzleCount = 3U;
        static constexpr uint16_t s_cu16InputStateSize = 256U;

        static const uint8_t *const s_cpu8PuzzleDataTablePointers[s_cu8PuzzleCount];
        static const uint8_t s_cau8Puzzle1Data[];
        static const uint8_t s_cau8Puzzle2Data[];
        static const uint8_t s_cau8Puzzle3Data[];

        tclsSolver();

        ~tclsSolver();
        
        void SetPuzzleId( const uint8_t cu8PuzzleId );

        void Solve( const bool (&crefabInputState)[PUZZLE_INPUT_COUNT], bool (&refabOutputState)[PUZZLE_OUTPUT_COUNT] );

    private:

        const uint8_t *m_pcu8OutputSectionPointer;
        const uint8_t *m_pcu8ComponentSectionPointer;
        uint8_t m_au8OutputRootNodeIndexes[PUZZLE_OUTPUT_COUNT];
        tclsNode *m_pComponentNodes;

};

#endif