// File: logic_puzzle.cpp
// Author: Tom Woolfrey
//
// Main entry point for Arduino sketch.
// setup() called once on MCU initialisation.
// loop() called repeatedly until power is pulled.

#include <stdint.h>
#include "Arduino.h"
#include "solver.hpp"

#define PUZZLE_ID 0U

// Flash periodically on pin 13 (which is connected to
// a debug LED) to indicate that the software is running.
#define DEBUG_LED_PIN 13
uint32_t g_LastDebugLedPulseTime = 0U;

static const uint8_t gcau8InputPins[PUZZLE_INPUT_COUNT] =
{
    0,
    1,
    2,
    3,
    4,
    5,
    6,
    7
};

static const uint8_t gcau8OutputPins[PUZZLE_OUTPUT_COUNT] =
{
    A0,
    A1,
    A2,
    A3,
    A4,
    A5,
    8,
    9
};

tclsSolver g_clsSolver;

void setup()
{
    for(uint8_t i = 0; i < PUZZLE_INPUT_COUNT; i++)
    {
        pinMode(gcau8InputPins[i], INPUT_PULLUP);
    }
    for(uint8_t i = 0; i < PUZZLE_OUTPUT_COUNT; i++)
    {
        pinMode(gcau8OutputPins[i], OUTPUT);
        digitalWrite(gcau8OutputPins[i], LOW);
    }
}

void loop()
{
    const uint32_t cu32CurrTime = millis();
    if((cu32CurrTime - g_LastDebugLedPulseTime) >= 1000U)
    {
        // Time has elapsed, toggle the debug LED
        digitalWrite(DEBUG_LED_PIN, !digitalRead(DEBUG_LED_PIN));
        g_LastDebugLedPulseTime = cu32CurrTime;
    }
    
    // Get input states
    bool abInputStates[PUZZLE_OUTPUT_COUNT];
    for(uint8_t i = 0U; i < PUZZLE_INPUT_COUNT; i++)
    {
        abInputStates[i] = (digitalRead(gcau8InputPins[i]) == HIGH);
    }

    // Pass input states through solver
    bool abOutputStates[PUZZLE_OUTPUT_COUNT];
    g_clsSolver.Solve( abInputStates, abOutputStates );

    // Write output states
    for(uint8_t i = 0U; i < PUZZLE_OUTPUT_COUNT; i++)
    {
        // If the bit in output state byte is set, write HIGH to the output
        digitalWrite(gcau8OutputPins[i], (abOutputStates[i] ? HIGH : LOW));
    }
}