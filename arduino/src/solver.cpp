// File: solver.cpp
// Author: Tom Woolfrey
//
// Puzzle solver class code file

#include "solver.hpp"
#include <stddef.h>

const uint8_t *const tclsSolver::s_cpu8PuzzleDataTablePointers[s_cu8PuzzleCount] =
{
    s_cau8Puzzle1Data,
    s_cau8Puzzle2Data,
    s_cau8Puzzle3Data,
};

const uint8_t tclsSolver::s_cau8Puzzle1Data[] =
{
    0U, 4U, 0U, 13U, 8U, 0U, 0U, 0U, 
    0U, 0U, 0U, 0U, 0U, 1U, 0U, 3U, 
    0U, 0U,
};

const uint8_t tclsSolver::s_cau8Puzzle2Data[] =
{
    0U, 4U, 0U, 13U, 8U, 0U, 0U, 0U, 
    0U, 0U, 0U, 0U, 0U, 1U, 0U, 3U, 
    1U, 0U,
};

const uint8_t tclsSolver::s_cau8Puzzle3Data[] =
{
    0U, 4U, 0U, 13U, 8U, 0U, 1U, 2U, 
    3U, 4U, 5U, 6U, 7U, 8U, 0U, 17U, 
    0U, 20U, 0U, 23U, 0U, 26U, 0U, 29U, 
    0U, 32U, 0U, 35U, 0U, 38U, 2U, 1U, 
    0U, 2U, 1U, 1U, 2U, 1U, 2U, 2U, 
    1U, 3U, 2U, 1U, 4U, 2U, 1U, 5U, 
    2U, 1U, 6U, 2U, 1U, 7U,
};

tclsSolver::tclsSolver() :
    m_pComponentNodes(nullptr)
{
}

tclsSolver::~tclsSolver()
{
    if(m_pComponentNodes != nullptr)
    {
        delete[] m_pComponentNodes;
    }
}

const uint8_t *GetComponentPtr(const uint8_t *const cpcu8ComponentTable,  const uint16_t u16Index)
{
    const uint8_t *ret = nullptr;
    const uint8_t cu8CompCount = *cpcu8ComponentTable;
    if(u16Index < cu8CompCount)
    {
        uint16_t compOffset = *reinterpret_cast<const uint16_t*>(cpcu8ComponentTable + 1 + (u16Index * 2));
        ret = cpcu8ComponentTable + compOffset;
    }

    return ret;
}

void tclsSolver::SetPuzzleId( const uint8_t cu8PuzzleId )
{
    if(cu8PuzzleId < s_cu8PuzzleCount)
    {
        // Deallocate previous memory if allocated
        if(m_pComponentNodes != nullptr)
        {
            delete[] m_pComponentNodes;
        }

        // Pre-load certain things into RAM to speed up future lookups

        // Calculate offsets for output and component section
        const uint16_t cu16OutputSectionOffset = (s_cpu8PuzzleDataTablePointers[cu8PuzzleId][0] << 8U) + (s_cpu8PuzzleDataTablePointers[cu8PuzzleId][1]);
        const uint16_t cu16ComponentSectionOffset = (s_cpu8PuzzleDataTablePointers[cu8PuzzleId][2] << 8U) + (s_cpu8PuzzleDataTablePointers[cu8PuzzleId][3]);

        // Store pointers to the sections
        m_pcu8OutputSectionPointer = s_cpu8PuzzleDataTablePointers[cu8PuzzleId] + cu16OutputSectionOffset;
        m_pcu8ComponentSectionPointer = s_cpu8PuzzleDataTablePointers[cu8PuzzleId] + cu16ComponentSectionOffset;

        const uint8_t cu8ComponentCount = m_pcu8ComponentSectionPointer[0U];

        // Allocate memory for components
        // Create node object for each component
        m_pComponentNodes = new tclsNode[cu8ComponentCount];

        // Populate descriptor pointers and create vector of Node object pointer
        for(uint8_t i = 0U; i < cu8ComponentCount; i++)
        {
            m_pComponentNodes[i].SetDescriptor( GetComponentPtr( m_pcu8ComponentSectionPointer, i ) );
        }

        // Create array of output root node indexes
        for(uint8_t i = 0U; i < PUZZLE_OUTPUT_COUNT; i++)
        {
            m_au8OutputRootNodeIndexes[i] = m_pcu8OutputSectionPointer[1 + i];
        }
    }
}

void tclsSolver::Solve( const bool (&crefabInputState)[PUZZLE_INPUT_COUNT], bool (&refu8OutputState)[PUZZLE_OUTPUT_COUNT] )
{
    // Reset all component objects internal state
    const uint8_t cu8ComponentCount = m_pcu8ComponentSectionPointer[0U];
    for(uint8_t i = 0U; i < cu8ComponentCount; i++)
    {
        m_pComponentNodes[i].Reset();
    }

    // Allow all components to determine their state. This could be done
    // by the loop below, but components are expected to be in the data table in increasing
    // dependency order. Doing it this way will reduce the number of recursive calls when
    // DetermineState is called on each output node
    for(uint8_t i = 0U; i < cu8ComponentCount; i++)
    {
        // Don't care about the return value of this call. State will be stored internally
        (void) m_pComponentNodes[i].DetermineState(crefabInputState, m_pComponentNodes, cu8ComponentCount);
    }

    // Determine state for each output
    for(uint8_t i = 0U; i < PUZZLE_OUTPUT_COUNT; i++)
    {
        refu8OutputState[i] = m_pComponentNodes[m_au8OutputRootNodeIndexes[i]].DetermineState(crefabInputState, m_pComponentNodes, cu8ComponentCount);
    }
}
