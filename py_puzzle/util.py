from puzzle import Puzzle, Node, NodeType
from os import listdir
from os.path import isfile, join

class PuzzleFitnessBaseError(Exception):
    pass


class TautologyError(PuzzleFitnessBaseError):
    pass


class FeedbackLoopError(PuzzleFitnessBaseError):
    pass


class DirectConnectionError(PuzzleFitnessBaseError):
    pass


def read_puzzle_from_file(filename):

    outputs = None

    with open(filename, 'r') as puzzle_file:
        file_contents = puzzle_file.read()
        # Exec the file as Python code, allowing only the limited set of puzzle types into the sandbox
        f_globals = {'__builtins__': {}, 'Puzzle': Puzzle, 'Node': Node, 'NodeType': NodeType}
        f_locals = {}

        exec(file_contents, f_globals, f_locals)

        outputs = f_locals['outputs']

    if not outputs:
        raise RuntimeError("No outputs specified in puzzle file")

    return Puzzle(outputs)

def read_puzzles_from_dir(dirname):

    file_list = [f for f in listdir(dirname) if isfile(join(dirname, f))]
    puzzles = [read_puzzle_from_file(join(dirname, f)) for f in file_list]
    return puzzles