import os
from puzzle import Puzzle, Node, NodeType

IMG_DIR = "img"
IMG_EXT = ".bmp"


def getImageFilename(imgname):
    return os.path.join(IMG_DIR, imgname + IMG_EXT)


class InputGraphic:

    outer_radius = 20
    inner_radius = 15

    def __init__(self):
        pass

    def setLocation(self, location):
        self.location = location

    def draw(self, window, state):
        color = (255,0,0) if state else (0,0,0)


class OutputGraphic:

    def __init__(self):
        pass

    def setLocation(self, location):
        self.location = location

    def draw(self, surface, state):
        pass


class GateGraphic:

    def __init__(self, node):
        self.node = node
        self.location = (0, 0)
        self.loadImage()

    def setLocation(self, loc):
        self.location = loc

    def draw(self, state):
        pass

    def getInputConnectionPoint(self, idx):
        pass

    def getOutputConnectionPoint(self):
        if self.node.getType() == NodeType.NOT:
            return ()


class NotGateGraphic(GateGraphic):
    imageFileName = "not_gate"
    imageSurface = None

    def loadImage(self):
        if not NotGateGraphic.imageSurface:
            # Load image and prepare surface
            pass

    def getSurface(self):
        return NotGateGraphic.imageSurface


class AndGateGraphic(GateGraphic):
    imageFileName = "and_gate"
    imageSurface = None

    def loadImage(self):
        if not AndGateGraphic.imageSurface:
            # Load image and prepare surface
            pass

    def getSurface(self):
        return AndGateGraphic.imageSurface


class NandGateGraphic(GateGraphic):
    imageFileName = "nand_gate"
    imageSurface = None

    def loadImage(self):
        if not NandGateGraphic.imageSurface:
            # Load image and prepare surface
            pass

    def getSurface(self):
        return NandGateGraphic.imageSurface


class OrGateGraphic(GateGraphic):
    imageFileName = "or_gate"
    imageSurface = None

    def loadImage(self):
        if not OrGateGraphic.imageSurface:
            # Load image and prepare surface
            pass

    def getSurface(self):
        return OrGateGraphic.imageSurface


class NorGateGraphic(GateGraphic):
    imageFileName = "nor_gate"
    imageSurface = None

    def loadImage(self):
        if not NorGateGraphic.imageSurface:
            # Load image and prepare surface
            pass

    def getSurface(self):
        return NorGateGraphic.imageSurface


class XorGateGraphic(GateGraphic):
    imageFileName = "xor_gate"
    imageSurface = None

    def loadImage(self):
        if not XorGateGraphic.imageSurface:
            # Load image and prepare surface
            pass

    def getSurface(self):
        return XorGateGraphic.imageSurface


class ConnectionGraphic:

    def __init__(self, source, sink, sink_connection):
        self.source = source
        self.sink = sink

    def draw(self):
        pass


SCREEN_MARGIN_H = 100
SCREEN_MARGIN_W = 100


class PuzzleGraphics:

    def __init__(self, screen_size, puzzle, debug=False):
        self.input_states = [False] * 8

        # Perform layout of puzzle

        self.gfx_inputs = []
        self.gfx_gates = []
        self.gfx_connections = []

        max_inputs = 0

        for c in puzzle.get_components(include_leaf_nodes=True):
            if c.getType() == NodeType.LITERAL:
                input_idx = c.getChildren()[0]
                if input_idx > max_inputs:
                    max_inputs += 1
            elif c.getType() == NodeType.NOT:
                self.gfx_gates.append(NotGateGraphic(c))
            elif c.getType() == NodeType.AND:
                self.gfx_gates.append(AndGateGraphic(c))
            elif c.getType() == NodeType.OR:
                self.gfx_gates.append(OrGateGraphic(c))
            elif c.getType() == NodeType.NAND:
                self.gfx_gates.append(NandGateGraphic(c))
            elif c.getType() == NodeType.NOR:
                self.gfx_gates.append(NorGateGraphic(c))
            elif c.getType() == NodeType.XOR:
                self.gfx_gates.append(XorGateGraphic(c))

        gfx_input_v_offset = ( screen_size[1] - (2 * 100) / max_inputs )
        for i in range(max_inputs):
            gfx_input = InputGraphic()
            gfx_input.setLocation((50, int(i * gfx_input_v_offset)))
            self.gfx_inputs.append(gfx_input)

    def update(self, input_states):
        self.input_states = input_states

    def draw(self, window):
        for i in range(len(self.gfx_inputs)):
            self.gfx_inputs[i].draw(window, self.input_states[i])
