#!/usr/bin/env python3

import sys
from util import read_puzzle_from_file

if len(sys.argv) != 2:
    print("Usage: arduino_gen.py <puzzle_file>")
    sys.exit(1)

try:
    puzzle = read_puzzle_from_file(sys.argv[1])
    ser = puzzle.serialize()

    byte_index = 0
    for b in ser:
        print('%dU, ' % b, end='')
        byte_index += 1
        if byte_index == 8:
            print()
            byte_index = 0

    print()
except RuntimeError as r:
    print("Invalid puzzle file: " + str(r))
