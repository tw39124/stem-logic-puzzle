#!/usr/bin/env python3

import click
import sys
from math import inf
from puzzle import Puzzle, Node, NodeType
from util import (read_puzzle_from_file, read_puzzles_from_dir,
                  TautologyError, FeedbackLoopError,
                  DirectConnectionError, PuzzleFitnessBaseError)
from sympy import Not, And, Or, Nand, Nor, Xor, Equivalent, symbols, satisfiable, sympify, 
from sympy.logic.boolalg import BooleanTrue

G_IDEAL = 15


def get_sympy_clause(node):
    if node.get_type() == NodeType.TRUE:
        return sympify(True)
    elif node.get_type() == NodeType.FALSE:
        return sympify(False)
    elif node.get_type() == NodeType.LITERAL:
        return symbols('x' + str(int(node._children[0])))
    elif node.get_type() == NodeType.NOT:
        return Not(get_sympy_clause(node._children[0]))
    elif node.get_type() == NodeType.AND:
        return And(get_sympy_clause(node._children[0]), get_sympy_clause(node._children[1]))
    elif node.get_type() == NodeType.OR:
        return Or(get_sympy_clause(node._children[0]), get_sympy_clause(node._children[1]))
    elif node.get_type() == NodeType.NAND:
        return Nand(get_sympy_clause(node._children[0]), get_sympy_clause(node._children[1]))
    elif node.get_type() == NodeType.NOR:
        return Nor(get_sympy_clause(node._children[0]), get_sympy_clause(node._children[1]))
    elif node.get_type() == NodeType.XOR:
        return Xor(get_sympy_clause(node._children[0]), get_sympy_clause(node._children[1]))
    elif node.get_type() == NodeType.XNOR:
        return Equivalent(get_sympy_clause(node._children[0]), get_sympy_clause(node._children[1]))


class PuzzleGen:
    pass

class PuzzleGenEvol(PuzzleGen):
    def __init__(self, initial_dir=None):
        self.population = read_puzzles_from_dir(initial_dir)

# Fitness criteria and weights
# assigned are:
# Puzzle is unsolvable [-Inf]
# Puzzle has feedback loops [-Inf]
# One or more outputs cannot be turned off (i.e. is a tautology) [-Inf]
# One or more outputs has no gate in path [-Inf]
#
# Close to G_IDEAL gates []
# Minimise number of solutions []
# Minimal number of outputs with same truth table as another []
class PuzzleFitness:

    def __init__(self, puzzle):
        self._puzzle = puzzle
        self._sympy_clauses = [get_sympy_clause(x) for x in puzzle.get_outputs()]

    def verify_no_feedback_loops(self):
        # Verify that there are no feedback loops in the puzzle
        try:
            for i in self._puzzle.get_outputs():
                node_stack = []
                for n in i.tree_visitor_dfs(node_stack):
                    pass
        except RecursionError:
            raise FeedbackLoopError("Puzzle has feedback loops")

    def verify_no_tautologies(self):
        # Verify that there are no tautologies in the puzzle (i.e. outputs that cannot be turned off)
        outputs = self._puzzle.get_outputs()
        for o in range(0, len(outputs)):
            sympy_clause = get_sympy_clause(outputs[o])
            inv_clause = Not(sympy_clause)
            if not satisfiable(inv_clause):
                raise TautologyError("Output %s with logic %s is a tautology" % (o, str(outputs[o])))

    def verify_no_direct_connections(self):
        outputs = self._puzzle.get_outputs()
        for o in range(0, len(outputs)):
            if outputs[o].get_type() == NodeType.LITERAL:
                raise DirectConnectionError("Output %s is a direct map of input %s" % (o, str(outputs[o])))

    def get_gate_count(self, gate_type=[]):
        gates = [x for x in self._puzzle.get_components(include_leaf_nodes=False)]
        return len(gates)

    def get_solutions(self):
        root_clause = And(self._sympy_clauses[0],
                          self._sympy_clauses[1],
                          self._sympy_clauses[2],
                          self._sympy_clauses[3],
                          self._sympy_clauses[4],
                          self._sympy_clauses[5],
                          self._sympy_clauses[6],
                          self._sympy_clauses[7])
        models = satisfiable(root_clause, all_models=True)
        return models

    def verify_puzzle(self):
        self.verify_no_feedback_loops()
        solution_gen = self.get_solutions()

        solutions = []
        for s in solution_gen:
            if not s:
                raise PuzzleFitnessBaseError("No puzzle solutions found")
            print("Found solution %s" % s)
            solutions.append(s)

        if len(solutions) == 0:
            raise PuzzleFitnessBaseError("No puzzle solutions found")

        self.verify_no_tautologies()
        self.verify_no_direct_connections()
        return solutions

    def calcFitness(self):
        try:
            solutions = self.verify_puzzle()
        # If any of the verification steps raise an error, fitness score is -Infinity
        except PuzzleFitnessBaseError as r:
            print(str(r))
            return -inf

        # Assuming verification steps pass, the starting fitness is 0
        fitness = 0

        # Determine fitness for closeness to G_IDEAL number of gates
        # Follows an inverse x^2 relationship
        print("Gate count: %s" % self.get_gate_count())
        delta_g_ideal = abs(self.get_gate_count() - G_IDEAL)
        fitness = fitness + (100 - (delta_g_ideal ** 2)/2)

        # Number of solutions
        # Follows an inverse linear relationship
        print("Solution count: %s" % len(solutions))
        fitness = fitness - ((len(solutions) - 1) * 5)

        # Number of outputs that have the same input truth table as another
        equivalence_count = 0
        for s1 in range(0, len(self._sympy_clauses)):
            for s2 in range(s1 + 1, len(self._sympy_clauses)):
                e = Equivalent(self._sympy_clauses[s1], self._sympy_clauses[s2])
                if type(e) == BooleanTrue:
                    equivalence_count += 1

        print("Equivalence count: %s" % equivalence_count)
        # Follows an inverse linear relationship
        fitness = fitness - (10 * equivalence_count)
        return fitness


@click.command("solutions")
@click.argument("puzzlefile")
def calculate_solutions(puzzlefile):
    try:
        puzzle = read_puzzle_from_file(puzzlefile)
        pf = PuzzleFitness(puzzle)
    except RuntimeError as r:
        print("Invalid puzzle file: " + str(r))
        sys.exit(1)

    try:
        models = pf.get_solutions()
        for m in models:
            print(m)
    except RecursionError:
        print("Error: puzzle contains feedback loops")
        sys.exit(1)


@click.command("fitness")
@click.argument("puzzlefile")
def calculate_fitness(puzzlefile):
    try:
        puzzle = read_puzzle_from_file(puzzlefile)
        pf = PuzzleFitness(puzzle)
    except RuntimeError as r:
        print("Invalid puzzle file: " + str(r))
        sys.exit(1)

    print("Puzzle fitness: {}".format(pf.calcFitness()))


@click.group('calculate')
def calculate_group():
    pass

@click.command("evol")
@click.option("i", "initial_dir")
def run_evol(initial_dir=None):
    puz_gen = PuzzleGenEvol(initial_dir)


@click.group('run')
def run_group():
    pass

@click.command("verify")
@click.argument("puzzlefile")
def verify_puzzle(puzzlefile):
    try:
        puzzle = read_puzzle_from_file(puzzlefile)
        pf = PuzzleFitness(puzzle)
    except RuntimeError as r:
        print("Invalid puzzle file: " + str(r))
        sys.exit(1)

    try:
        pf.verify_puzzle()
        print("Puzzle verification passed")
    except PuzzleFitnessBaseError as r:
        print(f"Puzzle verification failed: {str(r)}")
        sys.exit(1)

@click.group()
def cli():
    pass


calculate_group.add_command(calculate_solutions)
calculate_group.add_command(calculate_fitness)
cli.add_command(verify_puzzle)
cli.add_command(calculate_group)
cli.add_command(run_group)

if __name__ == "__main__":
    cli()
