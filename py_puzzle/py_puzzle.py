#!/usr/bin/env python3

import sys
import logging
from puzzle import Puzzle, Node, NodeType
import pyglet
from pyglet import gl
import gfx
import argparse

input_toggle_keys = [(pyglet.window.key._1, pyglet.window.key.NUM_1),
                     (pyglet.window.key._2, pyglet.window.key.NUM_2),
                     (pyglet.window.key._3, pyglet.window.key.NUM_3),
                     (pyglet.window.key._4, pyglet.window.key.NUM_4),
                     (pyglet.window.key._5, pyglet.window.key.NUM_5),
                     (pyglet.window.key._6, pyglet.window.key.NUM_6),
                     (pyglet.window.key._7, pyglet.window.key.NUM_7),
                     (pyglet.window.key._8, pyglet.window.key.NUM_8)]

argparser = argparse.ArgumentParser()
argparser.add_argument("--windowed", "-w", action='store_true')
argparser.add_argument("--loglevel", default=None)
argparser.add_argument("--logfile", default=None)
argparser.add_argument("puzzle_file")

namespace = argparser.parse_args()

if namespace.loglevel is not None:

    handlers = []
    stdOutHandler = logging.StreamHandler(stream=sys.stdout)
    handlers.append(stdOutHandler)

    if namespace.logfile is not None:
        fileHandler = logging.FileHandler(namespace.logfile)

    logging.basicConfig(  )

components = None
outputs = None

try:
    with open(sys.argv[1], 'r') as puzzle_file:
        file_contents = puzzle_file.read()

        try:
            # Exec the file as Python code, allowing only the limited set of puzzle types into the sandbox
            exec(file_contents) in {'__builtins__': {}}, {Puzzle, Node, NodeType}
        except Exception as e:
            print()
except Exception as e:
    print("Error opening puzzle file: " + str(e))
    sys.exit(1)

if components is not None and outputs is not None:

    puzzle = Puzzle( components, outputs )
    inputs = [False] * 8

    window = pyglet.window.Window(visible=False, fullscreen=(not namespace.windowed))

    # Perform layout of logic gates and signal nets for puzzle
    gfx_context = gfx.PuzzleGraphics(window.get_size(), puzzle)

    @window.event
    def on_draw():
        window.clear()
        gfx_context.draw(window)

    @window.event
    def on_key_press(symbol, modifiers):
        i = 0
        for k in input_toggle_keys:
            if symbol == k[0] or symbol == k[1]:

                print("Toggling input %d" % (i+1))
                # Toggle input and update state
                inputs[i] = not inputs[i]
                gfx_context.update(inputs)
                break

            i += 1

    logging.debug("Setup done. Starting game loop...")
    window.set_visible()
    pyglet.app.run()
    logging.debug("Exiting...bye!")

        #for event in pygame.event.get():
        #    if event.type == QUIT or (event.type == KEYDOWN and event.key == K_ESCAPE):
        #        cont = False
        #    elif event.type == KEYDOWN:
        #        # Number keys toggle input states
        #        if event.key == pygame.K_1:
        #            inputs[0] = not inputs[0]
        #        elif event.key == pygame.K_2:
        #            inputs[1] = not inputs[1]
        #        elif event.key == pygame.K_3:
        #            inputs[2] = not inputs[2]
        #        elif event.key == pygame.K_4:
        #            inputs[3] = not inputs[3]
        #        elif event.key == pygame.K_5:
        #            inputs[4] = not inputs[4]
        #        elif event.key == pygame.K_6:
        #            inputs[5] = not inputs[5]
        #        elif event.key == pygame.K_7:
        #            inputs[6] = not inputs[6]
        #        elif event.key == pygame.K_8:
        #            inputs[7] = not inputs[7]


else:

	print("Puzzle file missing either 'components' list or 'outputs' list")
	sys.exit(1)


