from enum import Enum, unique
import struct


@unique
class NodeType(Enum):
    FALSE = 0
    TRUE = 1
    LITERAL = 2
    NOT = 3
    AND = 4
    OR = 5
    NAND = 6
    NOR = 7
    XOR = 8
    XNOR = 9


LEAF_NODE_TYPES = [NodeType.FALSE, NodeType.TRUE, NodeType.LITERAL]


class Node(object):

    def __init__(self, node_type, children=[], name=None):
        self._name = name
        self._type = node_type
        self._children = []
        self._serialized_index = None
        self._state = None

        if children:
            self.append_children(children)

    def __str__(self):
        if self._type == NodeType.TRUE:
            return "T"
        elif self._type == NodeType.FALSE:
            return "F"
        elif self._type == NodeType.LITERAL:
            return "X" + str(int(self._children[0]))
        elif self._type == NodeType.NOT:
            return "~" + str(self._children[0])
        elif self._type == NodeType.AND:
            return "(" + str(self._children[0]) + " & " + str(self._children[1]) + ")"
        elif self._type == NodeType.OR:
            return "(" + str(self._children[0]) + " | " + str(self._children[1]) + ")"
        elif self._type == NodeType.NAND:
            return "~(" + str(self._children[0]) + " & " + str(self._children[1]) + ")"
        elif self._type == NodeType.NOR:
            return "~(" + str(self._children[0]) + " & " + str(self._children[1]) + ")"
        elif self._type == NodeType.XOR:
            return "(" + str(self._children[0]) + " ^ " + str(self._children[1]) + ")"
        elif self._type == NodeType.XNOR:
            return "~(" + str(self._children[0]) + " ^ " + str(self._children[1]) + ")"

    def append_children(self, children):
        if type(children) != list:
            raise TypeError("Expecting list of child nodes (or int for NodeType.LITERAL), "
                            "got {}".format(type(children)))

        if (self._type == NodeType.AND or
            self._type == NodeType.OR or
            self._type == NodeType.NAND or
            self._type == NodeType.NOR or
            self._type == NodeType.XOR or
            self._type == NodeType.XNOR):

            if len(self._children) + len(children) > 2:
                raise RuntimeError("Node: node type %s can have at most 2 children" % self._type)

        elif (self._type == NodeType.LITERAL or self._type == NodeType.NOT):
            if len(self._children) + len(children) > 1:
                raise RuntimeError("Node: node type %s can have at most 1 child" % self._type)

        elif (self._type == NodeType.TRUE or self._type == NodeType.FALSE):
            raise RuntimeError("Node: node type %s cannot have child nodes" % self._type)

        if (self._type == NodeType.LITERAL):
            if type(children[0]) != int:
                raise TypeError("Node: child must be of type 'int' for node type LITERAL")

        # Add specified children to member list
        self._children.extend(children)

    def get_type(self):
        return self._type

    def get_children(self):
        return self._children

    def get_tree_height(self):
        max_height = 0
        curr_height = 0
        for c in self._children:
            if type(c) == Node:
                curr_height = c.get_tree_height() + 1
                if curr_height > max_height:
                    max_height = curr_height
        return max_height

    def serialize(self, output_buffer, output_count):
        if self._serialized_index is not None:
            return self._serialized_index
        else:
            # Recursively serialize children
            child_indexes = []

            if self._type == NodeType.LITERAL:
                child_indexes.append(int(self._children[0]))
            else:
                for c in self._children:
                    child_indexes.append(c.serialize(output_buffer, output_count))
                    output_count += 1

            # Serialization of child nodes done. Now serialize this node into
            # the buffer
            output_buffer.extend(struct.pack(">B", int(self._type.value)))
            output_buffer.extend(struct.pack(">B", len(child_indexes)))

            for ci in child_indexes:
                output_buffer.extend(struct.pack(">B", ci))

            # Serialized index for this node is the number of nodes that have already been
            # serialized to the output buffer
            self._serialized_index = output_count
            return self._serialized_index

    def reset_serialization(self):
        self._serialized_index = None
        for c in self._children:
            if isinstance(c, Node):
                c.reset_serialization()

    def calculate(self, inputstates):
        if self._state is None:
            if self._type == NodeType.TRUE:
                self._state = True
            elif self._type == NodeType.FALSE:
                self._state = False
            elif self._type == NodeType.LITERAL:
                assert type(self._children[0]) == int
                self._state = inputstates[self._children[0]]
            elif self._type == NodeType.NOT:
                self._state = self._children[0].calculate(inputstates)
            elif self._type == NodeType.AND:
                self._state = self.count_true_children(inputstates) == len(self._children)
            elif self._type == NodeType.OR:
                self._state = self.count_true_children(inputstates) > 0
            elif self._type == NodeType.NAND:
                self._state = self.count_true_children(inputstates) != len(self._children)
            elif self._type == NodeType.NOR:
                self._state = self.count_true_children(inputstates) == 0
            elif self._type == NodeType.XOR:
                self._state = self.count_true_children(inputstates) == 1
            elif self._type == NodeType.XNOR:
                self._state = self.count_true_children(inputstatus) != 1

        return self._state

    def reset_state(self):
        self._state = None

    def count_true_children(self, inputstates):
        return len([x for x in self._children if x.calculate(inputstates)])

    def tree_visitor_dfs(self, visited=None, include_leaf_nodes=False):
        # Returns a generator that performs a depth-first search of this
        # node's sub-tree, adding each visited node to the provided
        # list as it is visited, and removing it afterwards (creating a
        # dynamic 'visit stack' of nodes which always contains the nodes
        # on the tree path from the root node to the currently yielded
        # node)

        if type(visited) == list:
            visited.append(self)

        yield self
        for i in range(0, len(self._children)):
            c = self._children[i]
            if type(c) == Node and (include_leaf_nodes or (c.get_type() not in LEAF_NODE_TYPES)):
                if type(visited) == list and c in visited:
                    raise RecursionError("Recursive error: node '%s' child[%s] refers to node '%s'" % (str(self), i, str(c)))
                yield from c.tree_visitor_dfs(visited)

        if type(visited) == list:
            visited.remove(self)


class Puzzle(object):

    def __init__(self, outputs):
        self._outputs = outputs

    def get_outputs(self):
        return self._outputs

    def get_components(self, include_leaf_nodes=False):
        # Traverse the tree for each output, building a list of node objects
        nodes = []
        for o in self._outputs:
            for c in o.tree_visitor_dfs(include_leaf_nodes=include_leaf_nodes):
                if c not in nodes:
                    nodes.append(c)

        return nodes

    def calculate(self, inputstates):
        output_states = [x.calculate(inputstates) for x in self._outputs]
        for component in self.get_components(include_leaf_nodes=True):
            component.reset_state()
        return output_states

    def get_height(self):
        max_height = 0
        for o in self._outputs:
            if o.get_tree_height() > max_height:
                max_height = o.get_tree_height()
        return max_height

    def serialize(self):
        output_root_indexes = [-1] * len(self._outputs)

        components = self.get_components(include_leaf_nodes=True)
        serialized_components = []

        output_count = 0
        for component_idx in range(0, len(components)):
            serialized = []
            c = components[component_idx]
            output_count = c.serialize(serialized, output_count)
            serialized_components.append(serialized)

            # Determine whether this component is a root node for an output
            for i in range(0, len(self._outputs)):
                if self._outputs[i] == c:
                    output_root_indexes[i] = component_idx

        for c in components:
            c.reset_serialization()

        # Build component section
        component_section = []
        component_section.extend(struct.pack(">B", len(serialized_components)))
        curr_offset = len(component_section) + (2 * len(serialized_components))
        for c in serialized_components:
            component_section.extend(struct.pack(">H", curr_offset))
            curr_offset += len(c)

        for c in serialized_components:
            component_section.extend(c)

        # Build output section
        output_section = []
        output_section.extend(struct.pack(">B", len(self._outputs)))

        for output_node_idx in output_root_indexes:
            output_section.extend(struct.pack(">B", output_node_idx))

        # Puzzle data is concatenation of output and component sections with
        # header containing offsets to each section
        header = []
        header.extend(struct.pack(">H", 4))
        header.extend(struct.pack(">H", 4 + len(output_section)))

        return header + output_section + component_section
